package com.employee;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.employee.domain.CreateEmployeeRequest;
import com.employee.domain.CreateEmployeeResponse;
import com.employee.domain.Employee;
import com.employee.repository.EmployeeRepository;
import com.employee.service.EmployeeService;

/**
 * Unit test for simple App.
 */
@RunWith(MockitoJUnitRunner.class)
public class AppTest {
	/**
	 * Rigorous Test :-)
	 */
	@Test
	public void shouldAnswerWithTrue() {
		assertTrue(true);
	}

	@Mock
	private EmployeeRepository employeeRepository;

	@Test
	public void shouldSaveEmployeeSuccefully() {
		CreateEmployeeRequest employeeRequest = new CreateEmployeeRequest();
		
		EmployeeService employeeService = mock(EmployeeService.class);
		
		employeeRequest.setName("Alex");
		employeeRequest.setAddress("USA");
		employeeRequest.setSalary(45000.00);
		employeeRequest.setPhone("348237383");
		employeeRequest.setEmail("alex@gmail.com");

		when(employeeRepository.save(any(Employee.class))).thenReturn(new Employee());

		CreateEmployeeResponse request = employeeService.createEmployee(employeeRequest);

//		assertThat(request.getName(), is("Alex"));
//		assertThat(request.getAddress(), is("USA"));
//		assertThat(request.getSalary(), is(45000.00));
//		assertThat(request.getPhone(), is("348237383"));
//		assertThat(request.getEmail(), is("alex@gmail.com"));

	}
	
	@Test
	public void checkNullValues() {
		
	}
}
