package com.employee.service;

import com.employee.domain.AllEmployeeResponse;
import com.employee.domain.CreateEmployeeRequest;
import com.employee.domain.CreateEmployeeResponse;
import com.employee.domain.DeleteEmployeeResponse;
import com.employee.domain.EmployeeResponse;
import com.employee.domain.UpdateEmployeeRequest;

public interface EmployeeService {

	public AllEmployeeResponse getAllEmployees();

	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest);

	public DeleteEmployeeResponse deleteEmployeeById(String employeeId);

	public EmployeeResponse updateEmployee(String employeeId, UpdateEmployeeRequest request);

	EmployeeResponse getEmployeeById(String employeeId);
}
