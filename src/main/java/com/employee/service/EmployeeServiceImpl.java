package com.employee.service;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.employee.domain.AllEmployeeResponse;
import com.employee.domain.CreateEmployeeRequest;
import com.employee.domain.CreateEmployeeResponse;
import com.employee.domain.DeleteEmployeeResponse;
import com.employee.domain.Employee;
import com.employee.domain.EmployeeResponse;
import com.employee.domain.UpdateEmployeeRequest;
import com.employee.domain.common.NotFoundException;
import com.employee.domain.transformer.EmployeeTransformer;
import com.employee.domain.util.EmployeeValidator;
import com.employee.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Autowired(required = false)
	private EmployeeRepository employeeRepository;

	@Autowired(required = false)
	private EmployeeValidator validator;

	@Override
	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest) {
		logger.info("Inside create employee service");

		Employee employee = null;
		CreateEmployeeResponse employeeResponse = null;

		try {
			employee = EmployeeTransformer.transformEmployeeRequestToDto(createEmployeeRequest);

			validator.assertEmployeeRequest(employee.getName(), employee.getAddress(), employee.getSalary(),
					employee.getPhone(), employee.getEmail());

			String employeeEmail = employee.getEmail();
			String employeePhone = employee.getPhone();

			if (validator.isValidPhone(employeePhone) && validator.isValidEmail(employeeEmail)) {

				employee = employeeRepository.save(employee);
				employeeResponse = EmployeeTransformer.transformEmployeeDtoToResponse(employee);

			}

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			e.printStackTrace();
		}

		logger.info("Successfully created the employee");
		return employeeResponse;
	}

	@Override
	public AllEmployeeResponse getAllEmployees() {
		logger.info("Inside get all employees service");

		List<Employee> employees = null;
		AllEmployeeResponse allEmployeeResponse = null;

		try {

			employees = employeeRepository.findAll();
			allEmployeeResponse = EmployeeTransformer.employeesToAllEmployeeResponse(employees);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new NotFoundException("Error while loading employees");
		}

		logger.info("Successfully retrieved all employees details");
		return allEmployeeResponse;
	}

	@Override
	public DeleteEmployeeResponse deleteEmployeeById(String employeeId) {
		logger.info("Inside delete employee by id service");

		DeleteEmployeeResponse deleteEmployeeResponse = null;
		Optional<Employee> optionalEmployee = null;

		optionalEmployee = employeeRepository.findById(employeeId);

		if (!optionalEmployee.isPresent()) {
			throw new NotFoundException("Employee not found for the given id");
		}

		try {

			employeeRepository.deleteById(employeeId);

		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.info("Successfully deleted the employee");
		return deleteEmployeeResponse;
	}

	@Override
	public EmployeeResponse updateEmployee(String employeeId, UpdateEmployeeRequest request) {
		logger.info("Inside update employee by id service");

		Employee existingEmployee = null;
		EmployeeResponse employeeResponse = null;

		try {
			if (!employeeRepository.findById(employeeId).isPresent()) {

				throw new NotFoundException("Employee not found for the given id");

			}

			existingEmployee = employeeRepository.findById(employeeId).get();

			if (StringUtils.hasText(request.getName())) {
				existingEmployee.setName(request.getName());
			}
			if (StringUtils.hasText(request.getAddress())) {
				existingEmployee.setAddress(request.getAddress());
			}
			if (request.getSalary() != null) {
				existingEmployee.setSalary(request.getSalary());
			}
			if (StringUtils.hasText(request.getPhone())) {

				if (validator.isValidPhone(request.getPhone())) {
					existingEmployee.setPhone(request.getPhone());
				} else {
					throw new NotFoundException("Phone number is not valid");
				}
			}
			if (StringUtils.hasText(request.getEmail())) {

				if (validator.isValidEmail(request.getEmail())) {
					existingEmployee.setEmail(request.getEmail());
				} else {
					throw new NotFoundException("Email is not valid");
				}
			}

			employeeRepository.save(existingEmployee);
			employeeResponse = EmployeeTransformer.employeeToEmployeeResponse(existingEmployee);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new NotFoundException("Error found when updating the employee.");
		}

		logger.info("Successfully updated the employee");
		return employeeResponse;
	}

	@Override
	public EmployeeResponse getEmployeeById(String employeeId) {
		logger.info("Inside get employee by id service");

		Optional<Employee> optionalEmployee = null;
		Employee employee = null;
		EmployeeResponse employeeResponse = null;

		optionalEmployee = employeeRepository.findById(employeeId);

		if (optionalEmployee.isPresent()) {
			employee = optionalEmployee.get();
		} else {
			throw new NotFoundException("Employee not found for the given id");
		}

		employeeResponse = EmployeeTransformer.employeeToEmployeeResponse(employee);

		logger.info("Successfully retrieved the employee by id");
		return employeeResponse;
	}
}
