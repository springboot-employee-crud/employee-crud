package com.employee.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.employee.controller.common.RequestMappings;
import com.employee.domain.AllEmployeeResponse;
import com.employee.domain.CreateEmployeeRequest;
import com.employee.domain.CreateEmployeeResponse;
import com.employee.domain.DeleteEmployeeResponse;
import com.employee.domain.EmployeeResponse;
import com.employee.domain.UpdateEmployeeRequest;
import com.employee.service.EmployeeService;

@Controller
public class EmployeeController {

	private final String EMPLOYEE_ENDPOINTS_RUNNING = "Employee CRUD endpoints are running";

	final static Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

	/*
	 * Home
	 */
	@RequestMapping(value = RequestMappings.CONTEXT_PATH, method = RequestMethod.GET)
	public String plainRequest() {
		return EMPLOYEE_ENDPOINTS_RUNNING;
	}

	/*
	 * Create employee
	 */
	@RequestMapping(value = RequestMappings.CREATE_EMPLOYEE, method = RequestMethod.POST)
	public @ResponseBody CreateEmployeeResponse addEmployee(@RequestBody CreateEmployeeRequest request) {

		logger.info("Inside create new employee endpoint");

		return employeeService.createEmployee(request);
	}

	/*
	 * Get all employees
	 */
	@RequestMapping(value = RequestMappings.GET_ALL_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody AllEmployeeResponse getAllEmployees() {

		logger.info("Inside get all employees endpoint");

		return employeeService.getAllEmployees();
	}

	/*
	 * Delete employee by id
	 */
	@RequestMapping(value = RequestMappings.DELETE_EMPLOYEE_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody DeleteEmployeeResponse deleteEmployeeResponse(
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside delete employee by id endpoint");

		return employeeService.deleteEmployeeById(employeeId);
	}

	/*
	 * Update employee by id
	 */
	@RequestMapping(value = RequestMappings.UPDATE_EMPLOYEE_BY_ID, method = RequestMethod.PUT)
	public @ResponseBody EmployeeResponse updateEmployeeById(@RequestBody UpdateEmployeeRequest request,
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside update employee by id endpoint");

		return employeeService.updateEmployee(employeeId, request);
	}

	/*
	 * Get employee by id
	 */
	@RequestMapping(value = RequestMappings.GET_EMPLOYEE_BY_ID, method = RequestMethod.GET)
	public @ResponseBody EmployeeResponse getEmployeeById(
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside get employee by id endpoint");

		return employeeService.getEmployeeById(employeeId);
	}
}
