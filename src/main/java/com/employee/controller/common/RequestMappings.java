package com.employee.controller.common;

public interface RequestMappings {

	public static String CONTEXT_PATH = "/";

	public static String CREATE_EMPLOYEE = "/addEmployee";

	public static String GET_ALL_EMPLOYEES = "/getAll";

	public static String UPDATE_EMPLOYEE_BY_ID = "/{emp_id}";

	public static String DELETE_EMPLOYEE_BY_ID = "/{emp_id}";

	public static String GET_EMPLOYEE_BY_ID = "/{emp_id}";
}
