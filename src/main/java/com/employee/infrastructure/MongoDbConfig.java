package com.employee.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
//@PropertySource("application.properties")
public class MongoDbConfig {

	@Value("${spring.data.mongodb.uri}")
	private String url;

//	@Value("${spring.data.mongodb.database}")
//	private String dbName;

//	@Bean
//	public MongoClient mongoClient() {
//		return MongoClients.create(url);
//	}
//
//	@Bean
//	public MongoTemplate mongoTemplate() throws Exception {
//		return new MongoTemplate(mongoClient(), dbName);
//	}

	@Bean
	public MongoClient mongoClient() {
		ConnectionString connectionString = new ConnectionString(url);
		MongoClientSettings clientSettings = MongoClientSettings.builder().applyConnectionString(connectionString)
				.build();

		return MongoClients.create(clientSettings);
	}
}
