package com.employee.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class AllEmployeeResponse {

	private List<EmployeeResponse> employeeResponses;

	public AllEmployeeResponse() {
	}
	
	public List<EmployeeResponse> getEmployees(){
		if (employeeResponses == null) {
			employeeResponses = new ArrayList<EmployeeResponse>();
		}
		return employeeResponses;
	}
	
	public void setEmployees(List<EmployeeResponse> employeeResponses) {
		this.employeeResponses = employeeResponses;
	}
}
