package com.employee.domain.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class EmployeeValidator {

	@Value("${regex.pattern.email}")
	private String emailRegex;

	@Value("${regex.pattern.phone}")
	private String phoneRegex;

	@Value("${regex.json.phone}")
	private String jsonPhone;

	public boolean isValidPhone(String phone) {
		Pattern pattern_phone = Pattern.compile(phoneRegex);
		Matcher matcher = pattern_phone.matcher(phone);
		return matcher.matches();
	}

	public boolean isValidEmail(String email) {
		Pattern pattern_email = Pattern.compile(emailRegex);
		Matcher matcher = pattern_email.matcher(email);
		return matcher.matches();
	}

	public boolean isValidPhoneNumber(String phone) {
		Pattern pattern = Pattern.compile(jsonPhone);
		Matcher matcher = pattern.matcher(phone);
		return matcher.matches();
	}

	public void assertEmployeeRequest(String name, String address, Double salary, String phone, String email) {

		Assert.hasText(name, "Employee name is required");
		Assert.hasText(address, "Employee address is required");
		Assert.notNull(salary, "Salary is required");
		Assert.hasText(phone, "Employee phone is required");
		Assert.hasText(email, "Employee email is required");

	}
}
