package com.employee.domain;

import org.springframework.stereotype.Component;

@Component
public class CreateEmployeeRequest {

	private String name;

	private String address;

	private Double salary;

	private String phone;
	
	private String email;

	public CreateEmployeeRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "CreateEmployeeRequest [name=" + name + ", address=" + address + ", salary=" + salary + "]";
	}
}
