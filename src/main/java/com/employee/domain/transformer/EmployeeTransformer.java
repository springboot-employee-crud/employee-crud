package com.employee.domain.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.employee.domain.AllEmployeeResponse;
import com.employee.domain.CreateEmployeeRequest;
import com.employee.domain.CreateEmployeeResponse;
import com.employee.domain.Employee;
import com.employee.domain.EmployeeResponse;

@Component
public class EmployeeTransformer {

public static Employee transformEmployeeRequestToDto(CreateEmployeeRequest createEmployeeRequest) {
		
		Employee employee = new Employee();
		
		employee.setEmpId(UUID.randomUUID().toString());
		employee.setName(createEmployeeRequest.getName());
		employee.setAddress(createEmployeeRequest.getAddress());
		employee.setSalary(createEmployeeRequest.getSalary());
		employee.setPhone(createEmployeeRequest.getPhone());
		employee.setEmail(createEmployeeRequest.getEmail());
		
		return employee;
	}
	
	public static CreateEmployeeResponse transformEmployeeDtoToResponse(Employee employee) {
		
		CreateEmployeeResponse employeeResponse = new CreateEmployeeResponse();
		
		employeeResponse.setName(employee.getName());
		employeeResponse.setAddress(employee.getAddress());
		employeeResponse.setSalary(employee.getSalary());
		employeeResponse.setPhone(employee.getPhone());
		employeeResponse.setEmail(employee.getEmail());
		
		return employeeResponse;
	}
	
	public static EmployeeResponse employeeToEmployeeResponse(Employee employee) {
		
		EmployeeResponse employeeResponse = new EmployeeResponse();
		
		employeeResponse.setName(employee.getName());
		employeeResponse.setAddress(employee.getAddress());
		employeeResponse.setSalary(employee.getSalary());
		employeeResponse.setPhone(employee.getPhone());
		employeeResponse.setEmail(employee.getEmail());
		
		return employeeResponse;
	}
	
	public static AllEmployeeResponse employeesToAllEmployeeResponse(List<Employee> employees) {
		
		AllEmployeeResponse  allEmployeeResponse = new AllEmployeeResponse();
		List<EmployeeResponse> employeeResponses = new ArrayList<EmployeeResponse>();
		
		for (Employee employee : employees) {
			EmployeeResponse  response = new EmployeeResponse();
			response.setName(employee.getName());
			response.setAddress(employee.getAddress());
			response.setSalary(employee.getSalary());
			response.setPhone(employee.getPhone());
			response.setEmail(employee.getEmail());
			employeeResponses.add(response); 
		}
		
		allEmployeeResponse.setEmployees(employeeResponses);
		return allEmployeeResponse;
	}
}
